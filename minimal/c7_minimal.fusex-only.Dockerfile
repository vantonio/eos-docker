#
# Simple EOS Docker file
#
# Version 0.1

FROM centos:7
LABEL maintainer="Fabio Luchetti, fabio.luchetti@cern.ch, CERN 2021"


# One of {citrine, diopside}
ARG EOS_CODENAME=citrine
# One of {commit, tag-testing, tag}
ARG REPOBRANCH=tag

# Expect to use eos-xrootd in running containers
ENV PATH /opt/eos/xrootd/bin:$PATH
ENV LD_LIBRARY_PATH /opt/eos/xrootd/lib64:$LD_LIBRARY_PATH

COPY eos-docker/minimal/el-7/epel.repo /etc/yum.repos.d/epel.repo
COPY eos-docker/minimal/el-7/eos.repo /etc/yum.repos.d/eos.repo
RUN sed -i "s|__EOS_CODENAME__|${EOS_CODENAME}|g" /etc/yum.repos.d/eos.repo \
    && yum-config-manager --enable "eos-${REPOBRANCH}"


RUN yum -y --nogpg install epel-release \
	&& yum -y --nogpg install \
    eos-fusex \
    && yum clean all && rm -rf /var/cache/yum

# Install some much needed utility: please bloat with care!
RUN yum -y --nogpg install nano && yum clean all && rm -rf /var/cache/yum

ENTRYPOINT ["/bin/bash"]
