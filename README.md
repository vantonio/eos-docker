## All in One ##

The EOS Docker image runs an all-in-one setup by default — all services run in one container.
The all-in-one setup can be used for testing other applications with EOS and is NOT recommended for production use.

To run all-in-one, use the command bellow.
```
docker run -h <hostname>.<domainname> --security-opt seccomp=unconfined --ulimit nofile=1024 --ulimit nproc=57875 --ulimit core=-1 --sysctl net.ipv6.conf.all.disable_ipv6=0 --privileged <image>
```

The `-h <hostname>.<domainname> ` option is required to prevent EOS from failing on startup.
```
error: when running without a configuration file you need to configure the EOS endpoint via fsname=<host>.<domain> - the domain has to be added!
```

The `--ulimit nofile=1024` option is required since EOS allocates memory at startup by a multiple of `nofile`. Docker inherits the `ulimits` from the host and some Linux distros have `nofile` set to `unlimited`, causing the EOS to be killed for memory exhaustion. With `strace` it's possible to observe EOS trying to allocate 10.737 GB with the mmap system call before being killed.
```
[root@eos /]# strace eos daemon run mq &

...

mmap(NULL, 10737418240, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS|MAP_NORESERVE, -1, 0) = 0x7f60e3400000
+++ killed by SIGKILL +++
```

The `--ulimit nproc=57875` option is required since Docker inherits the `ulimits` from the host and some Linux distros have `nproc` set to `unlimited` causing EOS to crash.

The `--security-opt seccomp=unconfined --ulimit core=-1 --sysctl net.ipv6.conf.all.disable_ipv6=0 --privileged` options are used by the [start_services.sh](https://gitlab.cern.ch/eos/eos-docker/blob/master/scripts/start_services.sh) script and are therefore recommended.